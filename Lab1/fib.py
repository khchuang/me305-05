# -*- coding: utf-8 -*-
'''
@file fib.py
@brief                  Fibonacci Number Calculator
@details                This function calculates the nth number of the Fibonacci sequence using the syntax "fib(n)". Note that the input must be a positive integer for the calculator to function properly.

Source code can be found here: https://bitbucket.org/khchuang/me305-05/src/master/Lab1/fib.py

@author                 Kyle Chuang
'''

def fib (idx):
    ''' 
    @brief This method calculates a Fibonacci number corresponding to a specified index.
    @return The Fibonacci number corresponding to index. 
    @param idx An integer specifying the index of the desired 
    Fibonacci number.
    '''     
    run = True
    while run:
        UserInput = input('Enter a Fibonacci number index or type "quit" to exit the program: ')
        if UserInput.isdigit():
            # ensures idx is an integer
            idx = int(UserInput)            
            # creates the first two terms of the list which populates as the loops iterate
            a = [0,1]
                        
            # output for fib(0)
            if idx == 0:
                print('Fibonacci Number = 0')
                            
            # output for fib(1)
            elif idx == 1:
                print('Fibonacci Number = 1')
                            
            # output for fib(positive integers)
            elif idx >= 0:
                for i in range (2, idx+1):
                    b = a[i-1] + a[i-2]
                    a.append(b)
                print(f'Fibonacci Number = {a[idx]}')
        elif UserInput == 'quit':
            run = False
        else:
            print('Please input a positive integer value.')   

            
if __name__ == '__main__':     
    
    fib(0)