# -*- coding: utf-8 -*-
"""
@file       FSM_enc.py
@brief      Encoder Finite State Machine
@details    This finite state machine is used to update the current position of 
            an encoder at a fixed interval. 
@author     Kyle Chuang
"""
import utime, shares
from enc import Encoder

class TaskEnc:
    '''
    @brief      Returns current position of the encoder
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT         = 0
    
    ## Constant defining state 1: Print Val
    S1_UPDATE       = 1    


    def __init__(self, interval, Encoder):

        '''
        @brief              A finite state machine to call the update() function.
        @param interval     A user input from class TaskEnc that represents the time interval to call update() at.
        '''      
 
        self.Encoder = Encoder

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval) 

    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function initializes the FSM, then will continually call
                    the "update()" function, which continuously updates the encoder
                    position at a fixed interval. 
        '''    
        
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_UPDATE)
                
                # For Debugging
                # print(str(self.runs) + ': State 0 ' + str(utime.ticks_diff(self.curr_time,self.start_time)))

            
            
            # State 1
            elif(self.state == self.S1_UPDATE):            
                
                self.Encoder.update()
                # print(self.Encoder.position) 
                
                if shares.cmd: #if there is user input
                    if shares.cmd == 122:
                        self.Encoder.set_position(0)
                        shares.resp = self.Encoder.position
                    elif shares.cmd == 112:
                        self.Encoder.get_position()
                        shares.resp = self.Encoder.position
                    elif shares.cmd == 100:
                        self.Encoder.get_delta()
                        shares.resp = self.Encoder.delta
                    else:
                        pass
                    shares.cmd = None
                else:
                    pass
                
                            
                # For Debugging
                # print(str(self.runs) + ': State 1 ' + str(utime.ticks_diff(self.curr_time,self.start_time)))
    

            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState