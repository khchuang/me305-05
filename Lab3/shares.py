'''
@file shares.py
@brief A container for all the inter-task variables
@author Kyle Chuang
'''

## The command character sent from the user interface task to the Encoder Task
cmd     = None

## The response from the Encoder task after encoding
resp    = None