# -*- coding: utf-8 -*-
"""
@file       FSM_UI.py
@brief      User Interface Finite State Machine
@details    This finite state machine is used query the user for input. 
            A "z" input will reset the counter to 0.
            A "p" input will print the last encoder position.
            A "d" input will print out the most recent encoder delta. 
@author     Kyle Chuang
"""
from pyb import UART
import utime
import shares
from FSM_enc import TaskEnc

class TaskUI:
    '''
    @brief      A finite state machine to obtain user inputs.
    @details    This class implements a finite state machine to query the user for 
                inputs without blocking the encoder FSM.
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT         = 0
    
    ## Constant defining state 1: Print Val
    S1_INPUT        = 1    
    
    ## Constant defining state 1: Print Val
    S2_RESPONSE     = 2      

    def __init__(self, interval, TaskEnc):
        '''
        @brief              Initializes the variables used for this class
        @param interval     A user input from class TaskEnc that represents the time interval 
                            that the User Interface FSM will update at. 
        '''
        
        self.TaskEnc = TaskEnc
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0

        self.interval = int(interval*1e6)     
        
        self.myuart = UART(2)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval) 

    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function initializes the FSM, then will continuously 
                    prompt the user for input (z, p, or d). Once the user presses
                    one of those keys, it will interface with the FSM_enc.py file to 
                    return the desired functions. 
        '''    
        
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):
                print('''
                User input required:
                  "z" - Zero the encoder position.
                  "p" - Print out the encoder position.
                  "d" - Print out the encoder delta.                     
                  ''')
                self.transitionTo(self.S1_INPUT)                
                
                # For Debugging
                #print(str(self.runs) + ': State 0 ' + str(utime.ticks_diff(self.curr_time,self.start_time)))
            
            
            # State 1
            elif(self.state == self.S1_INPUT):
                if self.myuart.any():                    
                    self.transitionTo(self.S2_RESPONSE)
                    shares.cmd =  self.myuart.readchar() #sends value to Nucleo
                
                # For Debugging
                # print(str(self.runs) + ': State 1 ' + str(utime.ticks_diff(self.curr_time,self.start_time)))                  
                  
                  
            # State 2      
            elif(self.state == self.S2_RESPONSE):                
                
                if shares.resp: 
                    #self.myuart.writechar(shares.resp)
                    print(shares.resp)
                    shares.resp = None
                    print('''
                        User input required:
                          "z" - Zero the encoder position.
                          "p" - Print out the encoder position.
                          "d" - Print out the encoder delta.                     
                          ''')
                else:
                    pass
                
                self.transitionTo(self.S1_INPUT)
                # For Debugging
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

