# -*- coding: utf-8 -*-
"""
@file       main_enc.py
@brief      Encoder Main File
@details    This main file uses functions from FSM_enc.py and FSM_UI.py to monitor
            the position of an encoder and interface with it accordingly. The 
            Encoder FSM continously updates the position of the encoder and the User 
            Interface FSM queries the user for inputs ("z", "p", or "d") and edits the
            position of the encoder accordingly. 
            
@author     Kyle Chuang
"""
from FSM_enc import TaskEnc
from FSM_UI import TaskUI
from enc import Encoder

myencoder = Encoder()

# Creating a task object using the button and motor objects above
task_a = TaskEnc(1, myencoder)

task_b = TaskUI(1, task_a)

# Run the tasks in sequence over and over again
while True: # effectively while(True):    
    task_a.run()
    task_b.run()
