# -*- coding: utf-8 -*-
'''
@file elevator.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator.

The user has two buttons to select floors.

There is also proximity sensors at either end of travel for the elevator
represented by "First" and "Second".
'''

from random import choice
import time

class Elevator:
    '''
    @brief      A finite state machine to control a two story elevator.
    @details    This class implements a finite state machine to control the
                operation of a two story elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                   = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN            = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP              = 2    
    
    ## Constant defining State 3
    S3_STOPPED_AT_FLOOR_1     = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_FLOOR_2     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING             = 5
        
    def __init__(self, interval, Button_1, Button_2, First, Second, Motor):
        '''
        @brief            Creates an Elevator object.
        @param First      An object from class Elevator representing the first floor
        @param Second     An object from class Elevator representing the second floor
        @param Button_1   An object from class Button representing the button that requests the first floor.
        @param Button_2   An object from class Button representing the button that requests the second floor. 
        @param Motor      An object from class MotorDriver representing the motor which drives the elevator.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the left limit
        self.Button_1 = Button_1
        
        ## The button object used for the right limit
        self.Button_2 = Button_2
        
        ## The button object used for the left limit
        self.First = First
        
        ## The button object used for the right limit
        self.Second = Second
        
        ## The motor object operating the elevator motor
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
      
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function transitions the elevator in between states 
                    and displays the status in command window. 
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            
            # S0 to S1
            if(self.state == self.S0_INIT):
                # Run State 0 Code. Transition to state 1
                print('Elevator On')
                self.transitionTo(self.S1_MOVING_DOWN)                
                self.Motor.Down(2)            
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            
            # S1 to S3
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code. Will automatically transition to state 5 after 10 seconds. 
                                               
                # Transitions to state 3 once at the first floor. Turns off motor
                print('Going Down!')
                if(self.First.getButton_State(1)): # Returns a random input from the first floor sensor
                    self.transitionTo(self.S3_STOPPED_AT_FLOOR_1) 
                    self.Motor.Stop(0)                    
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            
            # S2 to S4
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                print('Going Up!')
                if(self.Second.getButton_State(1)): #Returns a random input from the second floor sensor
                    self.transitionTo(self.S4_STOPPED_AT_FLOOR_2)
                    self.Motor.Stop(0)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            
            # S3 to S2 and stop conditions
            elif(self.state == self.S3_STOPPED_AT_FLOOR_1):
                print('Stopped at Floor 1')
                if(self.runs*self.interval > 10):  # if our task runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                    
                # If Button 2 is pressed on Floor 1
                if(self.Button_2.getButton_State(1)): # Returns a random input for Button 2
                    self.transitionTo(self.S2_MOVING_UP) # Elevator starts moving up
                    self.Button_2.getButton_State(0) # Clears Button 2
                    self.First.getButton_State(0) # Elevator is no longer on floor 1
                    self.Motor.Up(1)
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            
            # S4 to S1
            elif(self.state == self.S4_STOPPED_AT_FLOOR_2):
                print('Stopped at Floor 2')
                # If Button 1 is pressed on Floor 2
                if(self.Button_1.getButton_State(1)): # Returns a random input for Button 1
                    self.transitionTo(self.S1_MOVING_DOWN) # Elevator starts moving down
                    self.Button_1.getButton_State(0) # Clears Button 1
                    self.Second.getButton_State(0) # Elevator is no longer on floor 2
                    self.Motor.Down(2)
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            
                
            # Stop conditions
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print('Elevator Off')
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            #Error
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary passenger to use the elevator. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButton_State(self, val):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @param val  A value which is used to clear the inputs
        @return     A boolean representing the state of the button.
        '''
        if val == 1:
            return choice([True, False])
        else: 
            return False
    

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                operate. Forward is CCW and Reverse is CW. 
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self, vu):
        '''
        @brief      Moves the elevator upward
        @param vu   A value which is used to keep track of the motor status. 
                    When vu = 1, the elevator motor activates and the elevator 
                    moves upward. 
        '''
        if vu == 1:
            return choice ([True])
        else:
            return False
    
    def Down(self, vd):
        '''
        @brief      Moves the elevator downwards
        @param vd   A value which is used to keep track of the motor status. 
                    When vd = 2, the elevator motor activates and the elevator 
                    moves downward. 
        '''
        if vd == 2:
            return choice ([True])
        else:
            return False

    
    def Stop(self,vs):
        '''
        @brief Moves the motor forward
        @param vs   A value which is used to keep track of the motor status. 
                    When vs = 0, the elevator motor activates and the elevator 
                    moves downward. 
        '''
        if vs == 0:
            return choice ([True])
        else:
            return False




























