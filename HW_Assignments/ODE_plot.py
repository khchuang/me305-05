# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 10:16:59 2020

@author: kcdup
"""
import matplotlib.pyplot as plt
import numpy as np



def dbldot(x_k):
    k = 2
    a = 5
    x_dbldot = -(k + a*x_k**2)*x_k
    return x_dbldot


def x(x_k):
    T = np.arange(0, 10.01, 0.01)
    xdot_k = 0
    t = 0.01
    
    for i in range(len(T)):
        
        X = [x_k]
        Xdot = [0]
        Xdbldot = dbldot(X[i])
        Xdbldot
        
        x_k1 = X[i] + Xdot[i]*t + .5*Xdbldot*t**2        
        
        xdot_k1 = xdot_k + (dbldot(x_k) +dbldot(x_k1))*t/2
    
        X.append(x_k1) 
        
        x_k = x_k1
        
        xdot_k = xdot_k1
        
        return X
    
    plt.plot(T, X, 'ko') # like matlab documentation
    plt.xlabel('Time [s]')
    plt.ylabel('Theta [degrees]')
    plt.show
    np.savetxt('Example.csv', T, X, delimiter = ',')


if __name__ == '__main__':     
    
    x(0)
    
    




        
        
    

    
    
