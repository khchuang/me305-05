# -*- coding: utf-8 -*-
'''
@file main_elev.py
@brief      This is the main code used to run the Elevator FSM. 
@details    This simulates two independent elevators that move between two 
            floors simultaneously. User inputs are simulated as a random input
            as we do not have any hardware interfacing with this.
'''

from Elevator import Button, Elevator, MotorDriver


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
First_a = Button('PB5')
Second_a = Button('PB6')
Button_1a = Button('PB7')
Button_2a = Button('PB8')
Motor_a = MotorDriver()

First_b = Button('PC5')
Second_b = Button('PC6')
Button_1b = Button('PC7')
Button_2b = Button('PC8')
Motor_b = MotorDriver()


# Creating two task objects using the button and motor objects above
task_a = Elevator(0.1, Button_1a, Button_2a, First_a, Second_a, Motor_a)

task_b = Elevator(0.1, Button_1b, Button_2b, First_b, Second_b, Motor_b)

# Run the tasks in sequence over and over again
for N in range(17000000): # effectively while(True):    
    task_a.run()
    task_b.run()
