# -*- coding: utf-8 -*-
'''
@file mainpage.py

@mainpage 

@section sec_intro Introduction
Hello, welcome to my online portfolio! On here, I have documented my projects
and various coding assignments from ME 305, Introduction to Mechatronics.

Table of Contents:
    1. Lab 1: Fibonacci Sequence Generator
    2. HW 1: Two Story Elevator Simulation

Source Code Repository: https://bitbucket.org/khchuang/me305-05/src/master/

@page page_fib Fibonacci Sequence

@section sec_sum1 Summary

@details    In this lab, I wrote code to calculate the nth term of the Fibonacci
            seqeunce. The primary focus was to practice using Python, and also to gain
            practice uploading files to Bitbucket using Doxygen and Sourcetree. See 
            fib.py for further documentation about the code. 

Source code can be found here: https://bitbucket.org/khchuang/me305-05/src/master/Lab1/fib.py

@date October 1, 2020




@page page_elv Finite State Machine: Two-Story Elevator Simulation

@section sec_sum Summary

@details    In this homework assignment, I wrote code to simulate an elevator operating 
            between two floors (elevator.py). When run, the code simulates all the states the elevator
            will go through during opertaion:
    1. Moving Down
    2. Stopped at Floor 1
    3. Moving Up
    4. Stopped at Floor 2
    
The attached main file (main_elev.py) details how to import and use this code 
by simulating two elevators operating independently of each other. 
 
Source code can be found here: https://bitbucket.org/khchuang/me305-05/src/master/HW_Assignments/

@section sec_FSM State Transition Diagram

@details    Here is my state transition diagram that was used to outline the 
            algorithm for the elevator simulation. 
            
@image html pic.png

@date October 7, 2020
'''