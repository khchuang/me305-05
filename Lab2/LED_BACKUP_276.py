# -*- coding: utf-8 -*-
"""
@file LED.py
@brief      Blinking LED
@details    This finite state machine is used to simulate a blinking LED at a fixed interval.
            It will also simultaneously interface with a NucleoL476 board to light an LED
            following a sawtooth pattern. 
            
            The inputs of the function are TaskBlink(interval, real).
            "Interval" refers to the period over which the sawtooth will span, or the 
            frequency at which the virtual LED turns off and on. "Real" refers to which
            code the user wants run: 0 = Virtual LED, 1 = Physical LED
@author     Kyle Chuang
"""

import utime, pyb

class TaskBlink:
    '''
    @brief      A finite state machine to control a virtual and real LED.
    @details    This class implements a finite state machine to control the operation of
                two LEDs. One LED is virtual and the other is connected to a NucleoL476 board
                which will light up in sync with a sawtooth wave pattern
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT         = 0
    
    ## Constant defining state 1: Print Val
    S1_PRINT_VAL    = 1
    
    ## Constant defining state 2: LED_OFF
    S2_CLEAR        = 2
        
    def __init__(self, interval, real):
        '''
        @brief              Creates TaskBlink object.
        @param real         A user input from class TaskBlink that represents whether the system is physical or virtual.
        @param interval     A user input from class TaskBlink that represents the time interval that the LEDs operate over. 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        
        ## 0 = virtual, 1 = real
        self.real = real
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Counter that describes the intensity of the LED brightness
        self.counter = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next. NOTE: 91 included to account for 11 iterations. 1/11 = 90.9 ms
        self.next_time = utime.ticks_add(self.start_time, 91*self.interval)
               
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function transitions the elevator in between states 
                    and outputs the LED status in the command window (Virtual LED) or outputs
                    the LED brightness in the NucleoL476 board. The code will run 11 iterations
                    for each interval. This accounts for the brightness increase from 0 to 100
                    at increments of 10. 
        '''

        ## Micropython interface
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)        
        tim2 = pyb.Timer(2, freq = 20000)        
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        ## Main section of the code
<<<<<<< HEAD
        self.curr_time = utime.ticks_ms()
=======
        self.curr_time = utime.ticks_us()
>>>>>>> cb099502ab33961905a1a5427a1c560aea73c502
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):
                print('Virtual LED Initialized')
                self.transitionTo(self.S1_PRINT_VAL)

            
            ## State 1
            elif(self.state == self.S1_PRINT_VAL):                
                ## Virtual LED On
                if(self.real == 0):
                    self.counter += 10
                    ## After 11 iterations, transition turn the Virtual LED on 
                    if(self.counter == 110):
                        self.counter = 0
                        print('Virtual LED On') 
                        self.transitionTo(self.S2_CLEAR)
                
                ## Sawtooth waveform 
                elif(self.real == 1):
                    ## Each iteration of the task, increases the counter by 10
                    self.counter += 10
                    ## Micropython syntax for increasing the LED brightness
                    t2ch1.pulse_width_percent(self.counter)
                    ## Once the brightness reaches 100%, transition to state 2 (the clear)
                    if(self.counter == 100):
                        self.transitionTo(self.S2_CLEAR)
                    
            ## State 2
            elif(self.state == self.S2_CLEAR):                
                ## Virtual LED Off
                if(self.real == 0):
                    self.counter += 10
                    ## After 11 iterations, transition turn the Virtual LED off
                    if(self.counter == 110):  
                        self.counter = 0
                        print('Virtual LED Off')
                        self.transitionTo(self.S1_PRINT_VAL)
                
                ## Sawtooth waveform
                elif(self.real == 1):     
                    ## Reset Counter
                    self.counter = 0
                    t2ch1.pulse_width_percent(self.counter)
                    self.transitionTo(self.S1_PRINT_VAL)
            
            #Error
            else:
                # Invalid state code (error handling)
                pass                    
                    
                    
            self.runs +=1
            
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, 91*self.interval)
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

       
