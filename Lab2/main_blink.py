# -*- coding: utf-8 -*-
'''
@file       main_blink.py
@brief      This is the main code used to run the LED FSM.
@details    This simulates two independent LEDs that operate over a specified time interval [seconds].
            The first LED ("real" = 0) represents a simulated Virtual LED that blinks corresponding
            to the time interval. The second LED ("real" = 1) represents the LED housed
            in the NucleoL476 board that will light up in a sawtooth wave pattern
            with increasing brightness. 
            
            NOTE: Process will continue indefinitely until user interrupts the code. 
'''

from LED_Pyth import TaskBlink

# task = TaskBlink([interval, virtual/real])

task_a = TaskBlink(10, 0) # 0 = virtual

task_b = TaskBlink(10, 1) # 1 = real

# Run the tasks in sequence over and over again
while True: # effectively while(True):    
    task_a.run()
    task_b.run()
    
    
    
    