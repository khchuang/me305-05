# -*- coding: utf-8 -*-
"""
@file       main.py
@brief      Encoder Data Collection Main File
@details    This main file runs the UI_dataGen.py file off the Nucleo to collect 
            data. The data will be taken at a frequency of 5 Hz.
            
@author     Kyle Chuang
"""
from UI_dataGen import TaskData
from enc import Encoder

myencoder = Encoder()


# Creating a task object using the encoder object
task_a = TaskData(5, myencoder)


# Run the tasks in sequence over and over again
while True: # effectively while(True):    
    task_a.run()
