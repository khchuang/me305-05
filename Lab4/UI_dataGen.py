# -*- coding: utf-8 -*-
'''
@file       UI_dataGen.py
@brief      Back End Data Generation
@details    This is the code that will be loaded onto the Nucleo which will interface
            with the physical encoder via the enc.py file. It will record data
            at a frequency defined by the user in the main.py file. This code is
            designed to interpret the input sent from the front end. If the input
            is "G", it will record data for 10 seconds. If the input is "S", the 
            code will terminate and return to the state of waiting for user input. 
            The output will be formatted like "Time, Position" which will be sent
            to be read by the front end after each iteration of the task. 
            
@author     Kyle Chuang
'''
import utime
from pyb import UART
from enc import Encoder

class TaskData:
    '''
    @brief      Collects encoder data for 10 seconds
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT         = 0
    
    ## Constant defining state 1: Reading input from UI_front
    S1_INPUT        = 1  
    
    ## Constant defining state 2: Reading input from UI_front
    S2_COLLECT      = 2
    
    ## Constant defining state 3: Resets the counters and returns to State 1
    S3_STOP         = 3


    def __init__(self, frequency, Encoder):

        '''
        @brief              A finite state machine to record encoder position
        @param frequency    A user input that determines the frequency to collect data at.
        @param Encoder      Allows us to reference the Encoder class functions and data. 
        '''      
        self.myuart = UART(2)
        
        self.Encoder = Encoder
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(1e3/frequency)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## Time data variable
        self.time = 0 
        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval) 

    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function initializes the FSM, then will continually call
                    the "update()" function. If the input from the front end is 
                    "G" then the code will record the position and time for this 
                    iteration and print it so the front end can interpret the data. 
        '''
        
        self.curr_time = utime.ticks_ms()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):    
                #print('STATE 0')
                self.transitionTo(self.S1_INPUT)
                
            # State 1
            elif(self.state == self.S1_INPUT):
                if self.myuart.any() != 0: #if there is user input
                    val  = self.myuart.readchar()
                    if val == 71: # if the input is "G"
                        self.transitionTo(self.S2_COLLECT)
                        self.runs = -1
                        self.Encoder.set_position(0) #clear position
                    else:
                        self.transitionTo(self.S1_INPUT)
                else:
                    pass
    


            # State 2
            elif(self.state == self.S2_COLLECT):
                self.time = self.runs*self.interval
                self.Encoder.update()
                self.Encoder.get_position()
                #print('STATE 2')
                print('{:}, {:}'.format(self.time, self.Encoder.position)) #sends position as an updated position
                
                if (self.time >= 10000): # if our task runs for 10 seconds, transition to state
                    self.transitionTo(self.S3_STOP)
                
                elif self.myuart.any() != 0:
                    val = self.myuart.readchar()
                    if val == 83: #if the input is "s"
                        self.transitionTo(self.S3_STOP)            

            # State 3
            elif(self.state == self.S3_STOP):
                self.runs = -1
                self.transitionTo(self.S1_INPUT)
                                          
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
