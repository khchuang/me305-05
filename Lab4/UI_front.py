# -*- coding: utf-8 -*-
'''
@file       UI_front.py
@brief      Front End User Interface
@details    This is the front end code that the user will interface with in order
            to take data from the encoder. The code reads the encoder data from the 
            UI_dataGen.py file and appends it to a list. It then uses this data to
            plot a Position vs. Time graph and outputs the data into an Excel file
            titled "Encoder Position.csv". The user interface is controlled using
            "G" to begin data collection. Data collection will end automatically after 
            10 seconds or by pressing "S" while the code is running. 
            
            Note: The keyboard module is necessary for the code to run. Please 
            refer to the documentation here to install the module prior to running
            this code: https://pypi.org/project/keyboard/
            
@author     Kyle Chuang
'''
import serial, keyboard
import matplotlib.pyplot as plt
import numpy as np      


ser = serial.Serial(port='COM4', baudrate=9600, timeout=1)               
t = []
pos = []
    
def sendChar():
    '''
    @brief      Function that prompts for user input.
    @details    Prompts the user to input "G" in order to begin data collection
                from the encoder. It then writes "G" in ascii, which is read by
                the code on the Nucleo. 

    '''
    inv = input('Input "G" to begin sampling for 10 seconds. Press "S" at any time to cancel data collection! ')
    ser.write(str(inv).encode('ascii')) # Sends value to Nucleo 
    
    
    
def readChar():
    '''
    @brief      Function that consolidates the data from the UI_dataGen.py file.
    @details    This code will constantly be looking for any input data from the 
                Nucleo. Data is sent from the Nucleo in the form of 'Time, Position'.
                The code will take this, split up the vector, and then append each
                new data point to a list: t and pos. This will continue until the
                time limit (10 seconds) has passed, or until the letter "S" is 
                inputted in the keyboard. This data will then be plotted and 
                saved to an Excel file. 

    '''
    while True: 
        myval = ser.readline().decode('ascii')
        #A = myval.strip().split(',')
        
        if myval != 0:
            A = myval.strip().split(',')
            t.append(int(A[0])/1e3)
            pos.append(18*int(A[1])/35)
            myval = None
            if len(t) > 50:
                print('Data Collection Complete!')
                break
            if keyboard.is_pressed('S'):
                print('Data Collection Cancelled!')
                break


    plt.plot(t, pos)
    plt.xlabel('Time [s]')
    plt.ylabel('Position [degrees]')
    a = list(zip(t,pos))
    np.savetxt('Encoder Position.csv', a, delimiter = ',')
    

if __name__ == '__main__': 
    sendChar()
    readChar()


ser.close()
           
        


   
        
        
    


