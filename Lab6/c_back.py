# -*- coding: utf-8 -*-
'''
@file       c_back.py
@brief      Back End Data Generation
@details    This is the code that will be loaded onto the Nucleo which will interface
            with both c_front.py and ControllerTask.py. The back end uses 
            pySerial and UART to communicate with the front end. The back end uses 
            shares.py to set common variables between the controller task, which is also 
            loaded onto the Nucleo
            
@author     Kyle Chuang
'''
import utime, shares, struct
from pyb import UART


class TaskData:
    '''
    @brief      Sends and receives data from the front end and controller task
    @details    Receives a KP input from c_front.py. Generates the step input data.
                Sends KP and each step input data points to ControllerTask.py. 
                Receives omega and time data from ControllerTask.py for every iteration
                and then appends them to a list. Sends complete omega and time data lists
                to c_front.py for plotting. 
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT                         = 0
    
    ## Constant defining state 1: Reading input from UI_front
    S1_INPUT_FROM_FRONT             = 1  
    
    ## Constant defining state 2: Sending Kp and omega_ref to Controller task
    S2_SEND_TO_CONTROLLER           = 2
    
    ## Constant defining state 3: Receiving time and omega data. Append data
    S3_RECEIVE_FROM_CONTROLLER      = 3
    
    ## Constant defining state 4: Sending time and omega arrays to front end
    S4_SEND_TO_FRONT                = 4


    def __init__(self, interval):

        '''
        @brief              A finite state machine to record encoder position
        '''      
        self.myuart = UART(2)
        
        self.T = []
        
        self.O = []
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## Time data variable
        self.time = 0 
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval) 
        
        self.data = []

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_ms()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):    
                #print('STATE 0')
                self.transitionTo(self.S1_INPUT_FROM_FRONT)
                
            # State 1 to State 2
            elif(self.state == self.S1_INPUT_FROM_FRONT):
                self.counter = 0
                #print('STATE 1')
                if self.myuart.any() != 0: # if there is user input
                    val  = self.myuart.readline().decode('ascii')
                    shares.Kp = float(val)
                    # self.omega_ref = [data[1]]*500
                    self.omega_ref = [1500]*500
                    self.runs = -5
                    self.transitionTo(self.S2_SEND_TO_CONTROLLER) 
                
                # ## Testing just back end and the controller task
                # shares.Kp = 0.1
                # self.runs = -5
                # self.omega_ref = [1500]*500
                # self.transitionTo(self.S2_SEND_TO_CONTROLLER)                                    

            # State 2 to State 3
            elif(self.state == self.S2_SEND_TO_CONTROLLER):
                #print('STATE 2')
                shares.w_ref = self.omega_ref[self.counter]
                #print('omega ref =' + str(shares.w_ref))
                
                self.counter += 1
                #print(self.counter)
                #print(self.runs*self.interval)
                if (self.runs*self.interval) >= 5e3: 
                    #self.counter == len(self.omega_ref):
                    self.transitionTo(self.S4_SEND_TO_FRONT)
                
                else:
                    self.transitionTo(self.S3_RECEIVE_FROM_CONTROLLER)
                

            # State 3 to State 4
            elif(self.state == self.S3_RECEIVE_FROM_CONTROLLER):
                #print('STATE 3')
                if shares.w != None and shares.time != None:
                    #print('Omega = ' + str(shares.w))
                    #print('Time = ' + str(shares.time))
                    self.omegadata = self.O.append(shares.w)
                    shares.w = None
                    
                    self.timedata = self.T.append(shares.time)
                    shares.time = None
                    
                    self.transitionTo(self.S2_SEND_TO_CONTROLLER)
            
                
            # State 4 to State 1            
            elif(self.state == self.S4_SEND_TO_FRONT):
                #print('STATE 4')                
                self.myuart.write('{:};{:}\r\n'.format(self.T, self.O).encode('ascii'))
                self.T = []
                self.O = []
                if (self.runs*self.interval) >= 5e3: 
                    self.transitionTo(self.S1_INPUT_FROM_FRONT)
                                
            # Test back end and controller task    
            #     print(self.O)
            #     print(self.T)
            #     self.transitionTo(self.S5_DO_NOTHING)
            
            # elif(self.state == self.S5_DO_NOTHING):
                
            #     pass
                       
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


