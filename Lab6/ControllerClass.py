# -*- coding: utf-8 -*-
"""
@file       ControllerClass.py
@brief      Closed Loop Class
@details    Class Closed Loop takes KP reference omega and omega data as inputs to
            calculate and update the required duty cycle. It additionally takes into
            account saturation for duty cycles exceeding +/- 100%. 
@author     Kyle Chuang
"""

class ClosedLoop:
    '''
    @brief      Closed Loop Class
    '''

    def __init__(self):
        
        self.VDC = 3.3
        self.L_new = 0
        self.Kp = 0
        
    	## Constructor for encoder updater
    	#
    	#  Detailed info on encoder update method
    def update(self, w_ref, w):          
        '''
        @brief      Updates the motor duty cycle
        @details    This method takes reference motor speed and current motor speed
                    and calculates a new effort value for the motor to run at. This 
                    implements an integral controller to converge the two speeds as 
                    well as possible. 
                    
        '''
        self.Kp_prime = self.Kp/self.VDC
        self.w_ref = w_ref
        self.w = w
        
        if self.w_ref != None and self.w != None:
            self.L_old = self.Kp_prime*(self.w_ref - self.w)
            self.L_new += self.L_old
            
            # saturation
            if self.L_new > 100:
                self.L_new  = 100
                
            elif self.L_new  < -100:
                self.L_new  = -100
        
            #print('Duty cycle = ' + str(self.L_new))
        else:
            pass
 
    def get_Kp(self):
        '''
        @brief      Returns the current KP value
        '''
        return self.Kp    
    
    	## Zeros out the encoder
    	#
    	#  Detailed info on encoder zero function    
    def set_Kp(self, inpt):
        '''
        @brief      Resets the KP value to "inpt"
        @param inpt  A user input whose value is used to reset KP
        
        '''
        self.Kp = inpt


















