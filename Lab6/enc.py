# -*- coding: utf-8 -*-
"""
@file       enc.py
@brief      Encoder Class
@details    Class Encoder sets up the encoder parameters and functions which will 
            be used for future problems. These include update(), get_position(),
            set_position(), and get_delta(). 
@author     Kyle Chuang
"""

import pyb

class EncoderDriver:
    '''
    @brief      Encoder driver class
    @param pin1 Object creating the first pin of the encoder
    @param pin2 Object creating the second pin of the encoder
    @param tim  A user input which initializes the timer object. 
    '''

    def __init__(self, pin1, pin2, timer):
        
        # Initializes position counter
        self.position = 0
    
        # Initializes old counter
        self.old_count = 0    
        
        # Period = 65535
        self.period = 0XFFFF
        
        # Initializes timer
        self.tim = pyb.Timer(timer, prescaler = 0, period = 0XFFFF)
        
        # Creates two channels for Pins A6 and A7, which are used to connect the
        # encoder to the Nucleo board. 
        # self.tim.channel(1, pin = pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        # self.tim.channel(2, pin = pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
        
        self.tim.channel(1, pin = pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin = pin2, mode = pyb.Timer.ENC_AB)
    
    	## Constructor for encoder updater
    	#
    	#  Detailed info on encoder update method
    def update(self):          
        '''
        @brief      Updates the position of the encoder
        @details    This function updates the position of the encoder whenever it 
                    is called. This is done by taking the differnce of the last 
                    two positions (represented by "delta" variables) and checking for the validity
                    of the deltas. A "bad" delta, is defined when the two positions being 
                    referenced in the delta span across the encoder period
                    (going from 0 to 0XFFFF or 0XFFFF to 0). The code then adjusts
                    the "bad" delta by adding the period to the delta.
                    
                    The position is then updated, by continuously adding "good" 
                    deltas. 
                    
        '''
        
        # Initializes new_count
        self.new_count = self.tim.counter()
    
        # Subtracts two counts to find the raw delta value. 
        self.raw_delta = self.new_count - self.old_count   
        
        # Bad delta correction
        if abs(self.raw_delta) > self.period/2:
            
            # Overflow
            if self.raw_delta < 0: # new count reset to ~ 0, so raw delta is negative
                self.delta = self.raw_delta + self.period
            
            # Underflow
            elif self.raw_delta > 0: # new count reset to 65535. so raw delta is positive
                self.delta = self.raw_delta - self.period
        
        # All other deltas are "good" deltas
        else:
            self.delta = self.raw_delta
            
        # Gets position by summing all "good" deltas        
        self.position += self.delta           
        
        # Updates old_count to be the timer counter for this run. 
        self.old_count = self.tim.counter()
        
        
    	## Gets the encoder's position
    	#
    	#  Detailed info on encoder get_position method    
    def get_position(self):
        '''
        @brief      Returns the current position of the encoder
        '''
        return self.position
    
    
    	## Zeros out the encoder
    	#
    	#  Detailed info on encoder zero function    
    def set_position(self, pos):
        '''
        @brief      Resets the position of the encoder to the specified value "pos"
        @param pos  A user input whose value is used to reset the position of the encoder
        
        '''
        self.position = pos
        return self.position
    
    	## Zeros out the encoder
    	#
    	#  Detailed info on encoder zero function
    def get_delta(self):
        '''
        @brief      Returns the "good" deltas
        '''
        return self.delta

            

