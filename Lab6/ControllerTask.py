# -*- coding: utf-8 -*-
"""
@file       ControllerTask.py
@brief      Closed Loop FSM 
@details    This finite state machine takes KP and step data inputs from c_back.py
            and uses them as inputs to initialize and run the motor and encoder via 
            m_driver.py and enc.py. It then will constantly calculate duty cycle values
            using the update method from ControllerClass.py and updates the duty cycle
            at which the motor is running at in real time. Finally, the speed and time
            data are send back to ControllerClass.py using the shares.py file. 
            
@author     Kyle Chuang
"""
import utime, shares
from ControllerClass import ClosedLoop
from enc import EncoderDriver
from m_driver import MotorDriver


class TaskControl:
    '''
    @brief      Returns current position of the encoder
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT        = 0
    
    ## Constant defining state 1: Wait for Kp input
    S1_INPUT       = 1    
    
    ## Constant defining state 2: Closed Loop Control
    S2_CALC        = 2


    def __init__(self, interval, EncoderDriver, MotorDriver, ClosedLoop):

        '''
        @brief      A finite state machine to obtain time and motor speed
        @details    Kp is sent once from c_back.py and is set using the set_duty() method
                    from ControllerClass.py. Step input data is received after every
                    iteration from ControllerClass.py. The encoder update class from 
                    enc.py is called continuosly to obtain delta values from the encoder.
                    These values are used to calculate motor speed in real time which is 
                    used to calculate required duty cycle in ControllerClass.py. This duty
                    cycle is used to update the speed at which the motor is running at
                    using the set_duty() method from m_driver.py. Each iteration's 
                    time and omega data are then sent back to c_back.py via the share.py 
                    file. 
        '''      
 
        self.Encoder = EncoderDriver
        
        self.Motor = MotorDriver
        
        self.Controller = ClosedLoop
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval)

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''    
        
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):
                self.Encoder.set_position(0)
                self.transitionTo(self.S1_INPUT)
                
                # For Debugging
                # print(str(self.runs) + ': State 0 ' + str(utime.ticks_diff(self.curr_time,self.start_time)))

            # State 1
            elif(self.state == self.S1_INPUT):
                self.Motor.disable()
                
                if shares.Kp != None:
                    self.Controller.set_Kp(shares.Kp)
                    shares.Kp = None
                    self.runs = -1 
                    self.transitionTo(self.S2_CALC)

            
            # State 2
            elif(self.state == self.S2_CALC):
                self.Motor.enable()
                if shares.w_ref != 0:
                    self.omega_ref = shares.w_ref
                    shares.w_ref = None
                
                # Reads the encoder
                self.Encoder.update()
                
                # Sets shares.w as a new value each iteration. Outputs in rpm
                shares.w = (60000*self.Encoder.delta) / (4000*self.interval)
                
                # Calculates L
                self.Controller.update(self.omega_ref, shares.w)
                
             
                # Sets duty cycle to the output of L
                self.Motor.set_duty(self.Controller.L_new)
                
                shares.time = self.runs*self.interval
                
                # Will export data for 5 seconds
                if (shares.time >= 5e3):
                    self.transitionTo(self.S1_INPUT)
                
          
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState