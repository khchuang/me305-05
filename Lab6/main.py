# -*- coding: utf-8 -*-
"""
@file       main.py
@brief      Closed Loop Data Collection Main File
@details    This main file runs the c_back.py and ControllerTask.py files on the 
            Nucleo board at specified time intervals. This also initializes both 
            motors and encoders for future use, but for this lab I used motor and 
            encoder 2 exclusively. 
            
@author     Kyle Chuang
"""
import pyb
from enc import EncoderDriver
from m_driver import MotorDriver
from ControllerClass import ClosedLoop
from ControllerTask import TaskControl
from c_back import TaskData

''' MOTOR INITIALIZATION '''
# Motor 1 setup
pinA15 = pyb.Pin.cpu.A15
pinB4 = pyb.Pin.cpu.B4
ch1 = 1
pinB5 = pyb.Pin.cpu.B5
ch2 = 2

# Motor 2 setup
pinB0 = pyb.Pin.cpu.B0
ch3 = 3
pinB1 = pyb.Pin.cpu.B1
ch4 = 4

# Creates two objects which can independently control two separate motors
m1 = MotorDriver(pinA15, pinB4, ch1, pinB5, ch2, 3)
m2 = MotorDriver(pinA15, pinB0, ch3, pinB1, ch4, 3)


'''ENCODER INITIALIZATION '''
pinB6 = pyb.Pin.cpu.B6
pinB7 = pyb.Pin.cpu.B7

pinC6 = pyb.Pin.cpu.C6
pinC7 = pyb.Pin.cpu.C7

e1 = EncoderDriver(pinB6, pinB7, 4)
e2 = EncoderDriver(pinC6, pinC7, 8)

''' CONTROLLER INITIALIZATION '''
CL = ClosedLoop()


''' RUNNING TASKS '''
task1 = TaskControl(0.02, e2, m2, CL)
task2 = TaskData(0.01)

while True:
    task2.run()
    task1.run()
    
