# -*- coding: utf-8 -*-
'''
@file       FSM_blink.py
@brief      LED Blinking FSM
@details    This code contains a finite state machine that makes the LED on the nucleo
            blink at a specified frequency. The frequency is a user input from the 
            app interface. This input is used to modify the time interval per run.
            
            NOTE: Code currently does not use the methods defined in the BT_driver.py
            file due to errors. Instead, I have initialized the UART and portA5 information
            in this file and use the UART commands directly. I denote the functions that
            would have been used in the comments.
            
@author     Kyle Chuang
'''
import utime, pyb
from BT_driver import BT # UNUSED
from pyb import UART

class BTblink:
    '''
    @brief      Changes the LED frequency to the user input
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT         = 0
    
    ## Constant defining state 1: Reading input from UI_front
    S1_INPUT        = 1  
    
    ## Constant defining state 2: Reading input from UI_front
    S2_ON           = 2
    
    ## Constant defining state 3: Resets the counters and returns to State 1
    S3_OFF          = 3


    def __init__(self, BT):

        '''
        @brief              A finite state machine that edits LED frequency
        '''      
        ## Code from the BT_driver. CALLED BUT UNUSED
        self.BT = BT
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in milliseconds between runs of the task. Initializes
        ##  it to 0.1 seconds.
        self.interval = int(1e2)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## Time data variable
        self.time = 0 
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval) 
        
        
        ## Initializes UART
        self.myuart = UART(3, 9600)
        
        ## Initialize pinA5
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## Initialize frequency
        self.frequency = 0
        

    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function initializes the FSM, then will wait for a user 
                    input of frequency. The code will then turn the LED on and off
                    at the desired frequency. This will continue indefinitely until 
                    a new input is read, or until the code is terminated. 
        '''
        
        self.curr_time = utime.ticks_ms()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):    
                #print('STATE 0')
                self.transitionTo(self.S1_INPUT)
                
            # State 1
            elif(self.state == self.S1_INPUT):
                

                if self.myuart.any() != 0: #if there is user input
                # if self.BT.readany() != 0:  UNUSED
                    
                    # sets the frequency to the frequency input from the app.
                    # App sends values as 1 less than the specified value (i.e. 1Hz -> 0, 7Hz->6, etc)
                    self.frequency = int(self.myuart.readline()) + 1
                    # self.frequency = int(self.BT.readline()) + 1  UNUSED
                    
                    if self.frequency >= 1 and self.frequency <= 10:        # if the input between 1 and 10
                        self.interval = int(1e3/(2*self.frequency))         # sets the new interval to half of the new period
                        self.transitionTo(self.S2_ON)
                    self.myuart.write('LED is blinking at {:} Hz'.format(self.frequency)) # sends text to the App
                    # self.BT.write('LED is blinking at {:} Hz'.format(self.frequency))     UNUSED

            # State 2
            elif(self.state == self.S2_ON):
                
                if self.myuart.any() != 0:      # check if there is an additional input
                # if self.BT.readany() != 0: UNUSED
                
                    self.transitionTo(self.S1_INPUT)
                    
                else:# if there is no new input, LED on
                    self.pinA5.high()
                    # self.on()     UNUSED
                    
                    self.transitionTo(self.S3_OFF)
                    
            # State 3
            elif(self.state == self.S3_OFF):
                
                if self.myuart.any() != 0:      # check if there is an additional input
                # if self.BT.readany() != 0: UNUSED
                    
                    self.transitionTo(self.S1_INPUT)
                    
                else:                           # if there is no new input, LED off
                    self.pinA5.low()
                    # self.BT.off()     UNUSED
                    
                    self.transitionTo(self.S2_ON)                    
                                          
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
if __name__ == '__main__':   
        
    bluetooth = BT(0) # REFERENCED BUT UNUSED
    
    
    # Creating a task object using the encoder object
    task_a = BTblink(bluetooth)
    
    
    # Run the tasks in sequence over and over again
    while True: # effectively while(True):    
        task_a.run()

