# -*- coding: utf-8 -*-
"""
@file       BT_driver.py
@brief      Bluetooth Driver
@details    Class BT sets up the basic functions (write, read, any, and turning
            the LED on/off) for the LED light on the Nucleo board. 
@author     Kyle Chuang
"""

import pyb
from pyb import UART

class BT:
    '''
    @brief      Bluetooth driver class
    '''

    def __init__(self, val):
        '''
        @brief      Bluetooth driver class
        @details    Sets up UART port 3 at a baudrate of 9600. Additionally sets
                    up the LED on the Nucleo board
        '''
        self.val = val
        
        self.myuart = UART(3, 9600)
        
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    
    def write(self, inpt):          
        '''
        @brief      Will write any input to the function
                    
        '''
        self.myuart.write(inpt)
        
    def readline(self):
        '''
        @brief      Reads lines from an input
        '''
        val = self.myuart.readline()
        return val
                  
    def readany(self):
        '''
        @brief      Detects if there is an input
        '''
        val = self.myuart.any()
        return val
        
    def on(self):
        '''
        @brief      Turns LED on
        '''
        self.pinA5.high()
    
    def off(self):
        '''
        @brief      Turns LED off
        '''
        self.pinA5.low()

        
            
            

