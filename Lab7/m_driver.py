# -*- coding: utf-8 -*-
"""
@file       m_driver.py
@brief      Motor Driver
@details    This code sets up the motor driver for the ME405 board. This can be
            used to create two objects of this class that can independently control two
            separate motor drivers. 
@author     Kyle Chuang
"""
import pyb


class MotorDriver:
    '''
    @brief This class implements a motor driver for the ME405 board.
    '''
    def __init__ (self, nSLEEP_pin, IN1_pin, ch1, IN2_pin, ch2, timer):
        '''
        @details            Creates a motor driver by initializing GP10 pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param ch1          A pyb.Timer.Channel object that selects the first channel
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param ch2          A pyb.Timer.Channel object that tselects the second channel
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin
        '''
        # nSLEEP pin setup
        self.pinA15 = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinA15.low()
        
        # Initialize timer and pins       
        self.pin1 = pyb.Pin(IN1_pin)
        self.pin2 = pyb.Pin(IN2_pin)
        
        self.tim = pyb.Timer(timer, freq=20000)
        
        self.tchA = self.tim.channel(ch1, pyb.Timer.PWM, pin=self.pin1)
        self.tchB = self.tim.channel(ch2, pyb.Timer.PWM, pin=self.pin2)
        
    
    def enable(self):
        #print('Enabling Motor')
        self.pinA15.high()
    
    def disable(self):
        #print('Disabling Motor')
        self.pinA15.low()
        
    def set_duty(self, duty):
        '''
        @brief          Sets the duty cycle of the motor
        @details        This method sets the duty cycle to be sent to the motor to th
                        given motor to the given level. Positive values cause effort in
                        one direction, negative values in the opposite direction.
        @param duty     A signed integer holding the duty cycle of the PWM signal 
                        sent to the motor.
        '''
        self.tchA.pulse_width_percent(0)
        self.tchB.pulse_width_percent(0)
        
        if duty >= 0:
            self.tchA.pulse_width_percent(duty)
            
        elif duty <0:
            self.tchB.pulse_width_percent(abs(duty))

            
        
## Testing Motors and Encoders independently          
# if __name__ == '__main__':
    
#     # Motor 1 setup
#     pinA15 = pyb.Pin.cpu.A15
#     pinB4 = pyb.Pin.cpu.B4
#     ch1 = 1
#     pinB5 = pyb.Pin.cpu.B5
#     ch2 = 2
    
#     # Motor 2 setup
#     pinB0 = pyb.Pin.cpu.B0
#     ch3 = 3
#     pinB1 = pyb.Pin.cpu.B1
#     ch4 = 4
    
#     # Create the timer object used for PWM generation
#     tim = 3
    
#     # Creates two objects which can independently control two separate motors
#     m1 = MotorDriver(pinA15, pinB4, ch1, pinB5, ch2, tim)
#     m2 = MotorDriver(pinA15, pinB0, ch3, pinB1, ch4, tim)
    
#     # Enables the motor driver
#     m1.enable()
#     m2.enable()
    
#     # Sets the motor 1 duty cycle to -50%
#     m1.set_duty(-50)
#     # Sets the motor 2 duty cycle to 50%
#     m2.set_duty(50)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        