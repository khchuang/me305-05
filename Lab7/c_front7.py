# -*- coding: utf-8 -*-
'''
@file       c_front7.py
@brief      Front End User Interface Lab 7
@details    This is the front end code that the user will interface with in order
            to record motor speed and position data for 15 seconds. It accepts an
            input velocity profile (ref50.csv) which is used as the reference 
            velocity throughout the trial. The front end also calculates a performance 
            metric, J. The system responds optimally when J is minimized. 
            
@author     Kyle Chuang
'''
import serial, time
import matplotlib.pyplot as plt   


ser = serial.Serial(port='COM3', baudrate=115200, timeout = 1)

class UI:
    '''
    @brief      A finite state machine to operate the front end
    @details    This class implements a finite state machine to control the front
                end user interface. Each iteration will be plotted at the end and
                then prompt the user to input another KP value. 
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                     = 0    
    
   
    ## Constant defining State 1
    S1_SEND                     = 1    
    
    ## Constant defining State 2
    S2_RECEIVE                  = 2

    ## Constant defining State 3
    S3_CALC                     = 3    
    
    ## Constant defining State 4
    S4_PLOT                     = 4
    
        
    def __init__(self, interval):
        '''
        @brief            Creates the User Interface Object
        '''
        # self.ser = serial.Serial(port='COM3', baudrate=115200, timeout=1)  
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Counter which marks how many trials have been run
        self.iteration = 1
        
        ## Initializes J constant
        self.J = 0
        
        ## Initializes time reference data
        self.time = []
        
        ## Initializes velocity reference data
        self.velocity = []
        
        ## Initializes position reference data
        self.position = []
        
        ## Opens the reference csv file
        self.ref = open('ref50.csv');
        
        ## Interprets the data on the csv file. 
        while True:
            line = self.ref.readline()
            
            if line == '':
                break
            else:
                (t,v,x) = line.strip().split(',');
                self.time.append(float(t))
                self.velocity.append(float(v))
                self.position.append(float(x))
        
        self.ref.close()

        
      
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This function transitions the UI in between states 
                    and displays the inputs and plots in command window. 
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:            
            
            # S0 to S1
            if(self.state == self.S0_INIT):
                
                # Run State 0 Code. Transition to state 1
                self.transitionTo(self.S1_SEND)                          

           
            # S1 to S2
            elif(self.state == self.S1_SEND):
                
                # Clears values in between runs
                self.J = 0
                self.t = []
                self.omega = []
                self.pos = []
                self.t_datavals = 0
                self.o_datavals = 0
                self.p_datavals = 0
                
                # Run State 1 Code
                print('Run {:}'.format(self.iteration))
                self.inv = float(input('Kp = '))
                ser.write('{:f}\r\n'.format(self.inv).encode('ascii'))
                if self.inv != None:
                    self.runs = -1
                    self.transitionTo(self.S2_RECEIVE)
                            
            
            # S2 to S3
            elif(self.state == self.S2_RECEIVE):
                # Run State 2 Code
                if self.runs*self.interval >= 30:
                # if myval != None:
                    myval = ser.readline().decode('ascii')
                    data = myval.strip('[]\r\n').split('];[')
                    #print(data)
                    
                    self.t_datavals = data[0].split(',')
                    # print(self.t_datavals)
                    
                    self.o_datavals = data[1].split(',')
                    # print(self.o_datavals)
                    
                    self.p_datavals = data[2].split(',')
                    # print(self.p_datavals)
                    
                    
                    for i in range (len(self.t_datavals)):
                        self.t.append(int(self.t_datavals[i])/1000)
                        self.omega.append(float(self.o_datavals[i]))
                        self.pos.append(float(self.p_datavals[i]))
                        
                        myval = None 
                        
                    self.transitionTo(self.S3_CALC)
                
            
            # S3 to S4
            elif(self.state == self.S3_CALC):
                for n in range (len(self.t)):
                    self.J += (1/len(self.t))*((self.velocity[n] - self.omega[n])**2 + (self.position[n] - self.pos[n])**2)
                self.transitionTo(self.S4_PLOT)
            
            # S3 to S1
            elif(self.state == self.S4_PLOT):
                # print(self.t)
                # print(self.omega)
                # print(self.pos)

                plt.figure(self.iteration)
                plt.subplot(2,1,1)
                plt.plot(self.time, self.velocity)
                plt.plot(self.t[1:300], self.omega[1:300])
                plt.xlim(0, 15)
                plt.ylabel('Angular Velocity [RPM]')
                plt.legend(['Input', 'Response']) 
                plt.title('Response for K_P = {:}, J = {:}'.format(self.inv, self.J))
                
                plt.subplot(2,1,2)
                plt.plot(self.time, self.position)
                plt.plot(self.t[1:300], self.pos[1:300])
                plt.legend(['Input', 'Response']) 
                plt.xlim(0, 15)
                plt.xlabel('Time [s]')
                plt.ylabel('Position [deg]')
                
                
                self.iteration += 1
                self.inv = None
                self.transitionTo(self.S1_SEND)

            #Error
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 
    
    
    
if __name__ == '__main__':
    task1 = UI(0.01)    
    while True:
        task1.run()  
        
ser.close()
    

## Test code for two method direction

# ser = serial.Serial(port='COM3', baudrate=115200, timeout=1)

# def sendChar():
#     inv = float(input('Kp = '))
#     ser.write('{:f}\r\n'.format(inv).encode('ascii'))

    
# def readChar(rpm):
#     t = []
#     omega = []
    
#     time.sleep(5)
#     #while True:
#     myval = ser.readline().decode('ascii')
#     #print(myval)
#     if myval != None:
#         #data = [elem.strip('[]').split(';') for elem in myval]
#         data = myval.strip('[]\r\n').split('];[')
#         t_datavals = data[0].split(',')
#         o_datavals = data[1].split(',')
#         for i in range (len(t_datavals)):
#             t.append(int(t_datavals[i])/1000)
#             omega.append(float(o_datavals[i]))


#         myval = None
        
#     step = [rpm]*251
#     plt.plot(t, step)
#     plt.plot(t, omega)
#     plt.xlim(0, 5)
#     # plt.ylim(0, 1000)
#     plt.legend(['Input', 'Response'])
#     plt.xlabel('Time [s]')
#     plt.ylabel('Angular Velocity [RPM]')        
        
    
# if __name__ == '__main__':
#     sendChar()
#     readChar(800)


# ser.close()
    
    
    
    
    
    
    
    
    
    
    
    
    