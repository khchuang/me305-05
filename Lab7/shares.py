'''
@file shares.py
@brief A container for all the inter-task variables
@author Kyle Chuang
'''

## The reference angular velocity of the motor
w_ref   = None

## The angular velocity value of the motor
w       = None

## Kp input values
Kp      = None

## Position value of the motor
pos     = None

## Creates a time value to send to the backend
time    = None