# -*- coding: utf-8 -*-
'''
@file       c_back7.py
@brief      Back End Data Generation Lab 7
@details    This is the code that will be loaded onto the Nucleo which will interface
            with both c_front7.py and ControllerTask7.py. The back end uses 
            pySerial and UART to communicate with the front end. The back end uses 
            shares.py to set common variables between the controller task, which is also 
            loaded onto the Nucleo. This code is very similar to c_back.py, just slightly
            modified to generate reference data and accept position data. 
            
@author     Kyle Chuang
'''
import utime, shares
from pyb import UART
from array import array


class TaskData:
    '''
    @brief      Sends and receives data from the front end and controller task
    @details    Receives a KP input from c_front7.py. Generates the step input data.
                Sends KP and each step input data points to ControllerTask.py. 
                Receives omega and time data from ControllerTask.py for every iteration
                and then appends them to a list. Sends complete omega and time data lists
                to c_front7.py for plotting. 
    '''
    
    ## Constant defining state 0: Initialization
    S0_INIT                         = 0
    
    ## Constant defining state 1: Reading input from UI_front
    S1_INPUT_FROM_FRONT             = 1  
    
    ## Constant defining state 2: Sending Kp and omega_ref to Controller task
    S2_SEND_TO_CONTROLLER           = 2
    
    ## Constant defining state 3: Receiving time and omega data. Append data
    S3_RECEIVE_FROM_CONTROLLER      = 3
    
    ## Constant defining state 4: Sending time and omega arrays to front end
    S4_SEND_TO_FRONT                = 4
    
    S5_DO_NOTHING                   = 5


    def __init__(self, interval):

        '''
        @brief              A finite state machine to record encoder position
        ''' 
        
        self.myuart = UART(2)
        
        ## Initializes 
        self.T = []
        
        self.O = []
        
        self.P = []
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## Time data variable
        self.time = 0 
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next. 
        self.next_time = utime.ticks_add(self.start_time, self.interval)         
        
        ## Initializes a velocity array
        self.velocity = array('f', [])
        
        
        self.ref = open('ref50.csv');
        
        while True:
            line = self.ref.readline()
            
            if line == '':
                break
            else:
                (t,v,x) = line.strip().split(',');
                #v_prime = 10*v
                #self.time.append(float(t))
                self.velocity.append(float(v))
                #self.position.append(float(x))
        
        self.ref.close()

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_ms()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Initialization of the FSM
            if(self.state == self.S0_INIT):    
                #print('STATE 0')
                self.transitionTo(self.S1_INPUT_FROM_FRONT)
                
            # State 1 to State 2
            elif(self.state == self.S1_INPUT_FROM_FRONT):
                self.counter = 0
                #print('STATE 1')
                if self.myuart.any() != 0: # if there is user input
                    val  = self.myuart.readline().decode('ascii')
                    shares.Kp = float(val)
                    self.runs = -1
                    self.transitionTo(self.S2_SEND_TO_CONTROLLER) 
                
                # Testing just back end and the controller task
                # shares.Kp = 0.1
                # self.runs = -5
                # self.transitionTo(self.S2_SEND_TO_CONTROLLER)                                    

            # State 2 to State 3
            elif(self.state == self.S2_SEND_TO_CONTROLLER):
                #print('STATE 2')
                shares.w_ref = self.velocity[self.counter]
                # print('omega ref =' + str(shares.w_ref))
                
                self.counter += 1
                #print(self.counter)
                #print(self.runs*self.interval)
                if (self.runs*self.interval) >= 15e3: 
                # if self.counter >= len(self.velocity):
                    self.transitionTo(self.S4_SEND_TO_FRONT)
                
                else:
                    self.transitionTo(self.S3_RECEIVE_FROM_CONTROLLER)
                

            # State 3 to State 4
            elif(self.state == self.S3_RECEIVE_FROM_CONTROLLER):
                #print('STATE 3')
                if shares.w != None and shares.time != None and shares.pos != None:
                    #print('Omega = ' + str(shares.w))
                    #print('Time = ' + str(shares.time))
                    #print('Position = ' + str(shares.pos))
                    
                    self.O.append(shares.w)
                    shares.w = None
                    
                    #print(shares.time)
                    self.T.append(shares.time)
                    shares.time = None
                    
                    self.P.append(shares.pos)
                    shares.pos = None
                    
                    self.transitionTo(self.S2_SEND_TO_CONTROLLER)
            
                
            # State 4 to State 1            
            elif(self.state == self.S4_SEND_TO_FRONT):
                
                #print('STATE 4')                
                # print('{:};{:};{:}\r\n'.format(self.timedata, self.omegadata, self.positiondata))
                self.myuart.write('{:};{:};{:}\r\n'.format(self.T, self.O, self.P).encode('ascii'))
                # print(self.T)
                # print(self.O)
                # print(self.P)
                
                self.T = []
        
                self.O = []
                
                self.P = []
                
                # if (self.runs*self.interval) >= 5e3: 
                self.transitionTo(self.S1_INPUT_FROM_FRONT)
                # self.transitionTo(self.S5_DO_NOTHING)
                                
            # Test back end and controller task    
            
            # elif(self.state == self.S5_DO_NOTHING):                
            #     pass
                       
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


